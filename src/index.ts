import * as bunyan from 'bunyan';
import { ICreateLogger, ILoggerParams } from '../@types/index.d';

export const createLogger: ICreateLogger = (params?: ILoggerParams) => {
  const streams: bunyan.Stream[] = [
    {
      level: params.level || 'warn',
      stream: process.stdout
    }
  ];

  if (params.logsDir) {
    streams.push({
      count: params.count || Number(process.env.LOGGER_COUNT),
      level: params.level || Number(process.env.LOGGER_LEVEL),
      path: `${params.logsDir}/${params.name}.log`,
      period: params.period || process.env.LOGGER_PERIOD,
      type: 'rotating-file'
    });
  }

  return bunyan.createLogger({
    name: params.name,
    serializers: bunyan.stdSerializers,
    src: !!process.env.LOGGER_SRC,
    streams
  });
};

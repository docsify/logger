
import * as bunyan from 'bunyan';

interface ILoggerParams {
  name: string;
  logsDir?: string;
  level?: bunyan.LogLevelString | number;
  src?: boolean;
  count?: number;
  period?: string;
}
  
interface ICreateLogger {
  (params?: ILoggerParams): bunyan
}

export function createLogger(params?: ILoggerParams): bunyan

